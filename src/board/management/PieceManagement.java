package board.management;

import board.Board;
import controller.Controller;

public class PieceManagement {

    private int boardSize;

    public PieceManagement() {
        this.boardSize = Controller.getInstance().getBoard().getBoardSize();
    }

    /**
     * Method responsible for flipping the pieces.
     * @param board
     * @param color
     * @param row
     * @param column 
     */
    public void flipPieces(Board board, int color, int row, int column) {
        flipPiecesVertically(board, color, row, column);
        flipPiecesHorizontally(board, color, row, column);
        flipPiecesPrimaryDiagonal(board, color, row, column);
        flipPiecesSecondaryDiagonal(board, color, row, column);
        board.setPieceColor(row, column, color);
    }

    private boolean checkBounds(int number, int increment) {
        return number + increment < boardSize && number + increment >= 0;
    }

    private void flipPiecesVertically(Board board, int color, int row, int column) {
        int increment = 1;
        while (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == Math.signum(-color)) {
            increment++;
        }
        if (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == Math.signum(color)) {
            for (int i = 1; i < increment; i++) {
                board.setPieceColor(row + i, column, color);
            }
        }

        increment = -1;
        while (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == Math.signum(-color)) {
            increment--;
        }
        if (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == Math.signum(color)) {
            for (int i = -1; i > increment; i--) {
                board.setPieceColor(row + i, column, color);
            }
        }
    }

    private void flipPiecesHorizontally(Board board, int color, int row, int column) {
        int increment = 1;
        while (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == Math.signum(-color)) {
            increment++;
        }
        if (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == Math.signum(color)) {
            for (int i = 1; i < increment; i++) {
                board.setPieceColor(row, column + i, color);
            }
        }

        increment = -1;
        while (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == Math.signum(-color)) {
            increment--;
        }
        if (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == Math.signum(color)) {
            for (int i = -1; i > increment; i--) {
                board.setPieceColor(row, column + i, color);
            }
        }
    }

    private void flipPiecesPrimaryDiagonal(Board board, int color, int row, int column) {
        int incrementRow = -1, incrementColumn = -1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(-color)) {
            incrementRow--;
            incrementColumn--;
        }
        if (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)) {
            while (incrementRow < -1 && incrementColumn < -1) {
                incrementRow++;
                incrementColumn++;
                board.setPieceColor(row + incrementRow, column + incrementColumn, color);
            }
        }

        incrementRow = 1;
        incrementColumn = 1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(-color)) {
            incrementRow++;
            incrementColumn++;
        }
        if (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)) {
            while (incrementRow > 1 && incrementColumn > 1) {
                incrementRow--;
                incrementColumn--;
                board.setPieceColor(row + incrementRow, column + incrementColumn, color);
            }
        }
    }

    private void flipPiecesSecondaryDiagonal(Board board, int color, int row, int column) {
        int incrementRow = 1, incrementColumn = -1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(-color)) {
            incrementRow++;
            incrementColumn--;
        }
        if (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)) {
            while (incrementRow > 1 && incrementColumn < -1) {
                incrementRow--;
                incrementColumn++;
                board.setPieceColor(row + incrementRow, column + incrementColumn, color);
            }
        }

        incrementRow = -1;
        incrementColumn = 1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(-color)) {
            incrementRow--;
            incrementColumn++;
        }
        if (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)) {
            while (incrementRow < -1 && incrementColumn > 1) {
                incrementRow++;
                incrementColumn--;
                board.setPieceColor(row + incrementRow, column + incrementColumn, color);
            }
        }
    }
}
