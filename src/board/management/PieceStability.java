package board.management;

import board.Board;
import static board.management.BoardManagement.resetBoard;
import controller.Controller;

public class PieceStability {

    public static Board stablePieces(Board board) {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {

                if (board.wasUpdated()) {
                    resetBoard(board);
                    verifyPiece(board, i, j);
                } else {
                    verifyPiece(board, i, j);
                }
            }
        }
        return board;
    }

    private static int verifyPiece(Board board, int x, int y) {

        if (x < 0 || x > 7 || y < 0 || y > 7) {
            return 2;
        }

        int pieceValue = board.getPieceColor(x, y);
        int tempValue;
        int vertStability = 0;
        int horiStability = 0;
        int primDiagStability = 0;
        int secDiagStability = 0;

        //Se não for vazio, uma peça estável, uma peça não estável, nem borda
        if (!(pieceValue == 0) && !((pieceValue % 5) == 0) && !(pieceValue == 2)) {

            pieceValue *= 5;
            board.setPieceColor(x, y, pieceValue);

            //Começo das comparações principais
            tempValue = verifyPiece(board, x - 1, y + 1);

            primDiagStability = verifyStability(primDiagStability, pieceValue, tempValue);

            tempValue = verifyPiece(board, x - 1, y);

            vertStability = verifyStability(vertStability, pieceValue, tempValue);

            tempValue = verifyPiece(board, x - 1, y - 1);

            secDiagStability = verifyStability(secDiagStability, pieceValue, tempValue);

            tempValue = verifyPiece(board, x, y - 1);

            horiStability = verifyStability(horiStability, pieceValue, tempValue);
            //Fim das comparações principais

            //Se não é estável nesta direção
            if (primDiagStability < 5) {
                tempValue = verifyPiece(board, x + 1, y - 1);
                primDiagStability = verifyStability(primDiagStability, pieceValue, tempValue);

            }

            if (vertStability < 5) {
                tempValue = verifyPiece(board, x + 1, y);
                vertStability = verifyStability(vertStability, pieceValue, tempValue);
            }

            if (secDiagStability < 5) {
                tempValue = verifyPiece(board, x + 1, y + 1);
                secDiagStability = verifyStability(secDiagStability, pieceValue, tempValue);
            }

            if (horiStability < 5) {
                tempValue = verifyPiece(board, x, y + 1);
                horiStability = verifyStability(horiStability, pieceValue, tempValue);
            }

            //Se a peça não for completamente instável em alguma direção
            if (primDiagStability > 1 && vertStability > 1 && secDiagStability > 1 && horiStability > 1) {

                //Se a peça não é estável nesta direção
                if (primDiagStability != 5) {
                    if (verifyPrimDiag(board, x, y)) {
                        primDiagStability = 5;
                    }
                }

                if (vertStability != 5) {
                    if (verifyColumn(board, y)) {
                        vertStability = 5;
                    }
                }

                if (secDiagStability != 5) {
                    if (verifySecDiag(board, x, y)) {
                        secDiagStability = 5;
                    }
                }

                if (horiStability != 5) {
                    if (verifyRow(board, x)) {
                        horiStability = 5;
                    }
                }

                if (primDiagStability == 5 && vertStability == 5 && secDiagStability == 5 && horiStability == 5) {
                    pieceValue *= 5;
                    board.setPieceColor(x, y, pieceValue);
                    board.setUpdated(true);
                    return pieceValue;
                }
            }

            return pieceValue;
        } else {
            return pieceValue;
        }

    }

    private static int verifyStability(int tempStability, int pieceValue, int tempValue) {
        int stability = tempStability;
        if (tempValue != 0) {
            //Se é estável e da mesma cor ou borda
            if ((tempValue % 25 == 0 && Math.signum(pieceValue) == Math.signum(tempValue)) || tempValue == 2) {
                return stability = 5;
            } else {
                return stability += 1;
            }
        } else {
            return stability;
        }
    }

    private static boolean verifyRow(Board board, int x) {
        for (int i = 0; i < 8; i++) {
            if (board.getPieceColor(x, i) == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean verifyColumn(Board board, int y) {
        for (int i = 0; i < 8; i++) {
            if (board.getPieceColor(i, y) == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean verifyPrimDiag(Board board, int x, int y) {
        //Vai encontrar o início da diagonal
        while (board.isInBounds(x, y)) {
            x++;
            y--;
        }

        x--;
        y++;

        while (board.isInBounds(x, y)) {
            if (board.getPieceColor(x, y) == 0) {
                return false;
            }
            x--;
            y++;
        }
        return true;
    }

    private static boolean verifySecDiag(Board board, int x, int y) {
        //Vai encontrar o início da diagonal
        while (board.isInBounds(x, y)) {
            x--;
            y++;
        }

        x++;
        y--;

        while (board.isInBounds(x, y)) {
            if (board.getPieceColor(x, y) == 0) {
                return false;
            }
            x++;
            y--;
        }
        return true;
    }

    private static boolean verifyFilledRow(int x) {
        Board board = Controller.getInstance().getBoard();
        for (int i = 0; i < board.getBoardSize(); i++) {
            if (board.getPieceColor(x, i) == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean verifyFilledSquare(int x, int y) {
        Board board = Controller.getInstance().getBoard();

        return ((verifyFilledVertical(x, y) && verifyFilledHorizontal(x, y) && verifyFilledSecondaryDiagonal(x, y) && verifyFilledMainDiagonal(x, y)));
    }

    private static boolean verifyFilledVertical(int x, int y) {
        Board board = Controller.getInstance().getBoard();
        if (y > 0) {
            if (board.getPieceColor(x, y - 1) == 0) {
                return false;
            }
        }
        if (y < board.getBoardSize() - 1) {
            if (board.getPieceColor(x, y + 1) == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean verifyFilledHorizontal(int x, int y) {
        Board board = Controller.getInstance().getBoard();
        int[][] auxBoard = board.getBoard();
        if (x > 0) {
            if (auxBoard[x - 1][y] == 0) {
                return false;
            }
        }
        if (x < board.getBoardSize() - 1) {
            if (auxBoard[x + 1][y] == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean verifyFilledMainDiagonal(int x, int y) {
        Board board = Controller.getInstance().getBoard();
        int[][] auxBoard = board.getBoard();
        if (x > 0) {
            if (y > 0) {
                if (auxBoard[x - 1][y - 1] == 0) {
                    return false;
                }
            }
        }

        if (x < board.getBoardSize() - 1) {
            if (y < board.getBoardSize() - 1) {
                if (auxBoard[x + 1][y + 1] == 0) {
                    return false;

                }
            }
        }
        return true;
    }

    private static boolean verifyFilledSecondaryDiagonal(int x, int y) {
        Board board = Controller.getInstance().getBoard();
        int[][] auxBoard = board.getBoard();
        if (x < board.getBoardSize() - 1) {
            if (y > 0) {
                if (auxBoard[x + 1][y - 1] == 0) {
                    return false;
                }
            }
        }

        if (x > 0) {
            if (y < board.getBoardSize() - 1) {
                if (auxBoard[x - 1][y + 1] == 0) {
                    return false;

                }
            }
        }
        return true;
    }

    private static boolean verifyAdjacentRows(int x) {

        if (x == 0) {
            return ((!verifyFilledRow(x)) || (!verifyFilledRow(x + 1)));
        }

        if (x == Controller.getInstance().getBoard().getBoardSize() - 1) {
            return ((!verifyFilledRow(x - 1)) || (!verifyFilledRow(x)));
        }

        return ((!verifyFilledRow(x - 1)) || (!verifyFilledRow(x)) || (!verifyFilledRow(x + 1)));
    }
}
