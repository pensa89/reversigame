package board.management;

import board.Board;
import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BoardManagement {

    private Board board;
    private int boardSize;

    public BoardManagement(Board board) {
        this.board = board;
        this.boardSize = this.board.getBoardSize();
    }

    public void setBoard(Board newBoard) {
        this.board = newBoard;
    }

    public static void resetBoard(Board board) {
        for (int i = 1; i < 8; i++) {
            for (int j = 1; j < 8; j++) {
                int pieceValue = board.getPieceColor(i, j);
                //Se é 5 ou -5
                if (pieceValue % 5 == 0 && pieceValue % 25 != 0 && pieceValue != 0) {
                    board.setPieceColor(i, j, pieceValue / 5);
                }
            }
        }
        board.setUpdated(false);
    }

    public Set<Point> possibleMoves(Board board, int color) {

        Set<Point> possibleMoves = new HashSet();

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (board.getPieceColor(i, j) == 0 && checkNeighborhoodForPieces(i, j)) {
                    possibleMoves.add(new Point(i, j));
                }
            }
        }

        Set<Point> validMoves = new HashSet();

        for (Iterator<Point> iterator = possibleMoves.iterator(); iterator.hasNext();) {
            Point p = iterator.next();
            int row = p.x, column = p.y;

            if (checkMovementVertically(color, row, column) || checkMovementHorizontally(color, row, column)
                    || checkMovementPrimaryDiagonal(color, row, column) || checkMovementSecondaryDiagonal(color, row, column)) {
                validMoves.add(p);
            }
        }
        return validMoves;
    }

    private boolean checkNeighborhoodForPieces(int row, int column) {
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (row + i >= 0 && row + i < boardSize && column + j >= 0 && column + j < boardSize && board.getPieceColor(row + i, column + j) != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkBounds(int number, int increment) {
        return (number + increment < boardSize) && (number + increment >= 0);
    }

    /* checkBounds method checks if the row or column plus an increment is within bounds.
     The second part of the while condition checks if the sign of the color argument is opposite to 
     its color argument */
    private boolean checkMovementVertically(int color, int row, int column) {
        int increment = 1;
        while (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == -Math.signum(color)) {
            increment++;
        }
        if (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == Math.signum(color) && increment != 1) {
            return true;
        }

        increment = -1;
        while (checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == -Math.signum(color)) {
            increment--;
        }
        return checkBounds(row, increment) && Math.signum(board.getPieceColor(row + increment, column)) == Math.signum(color) && increment != -1;
    }

    private boolean checkMovementHorizontally(int color, int row, int column) {
        int increment = 1;
        while (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == -Math.signum(color)) {
            increment++;
        }
        if (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == Math.signum(color) && increment != 1) {
            return true;
        }

        increment = -1;
        while (checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == -Math.signum(color)) {
            increment--;
        }
        return checkBounds(column, increment) && Math.signum(board.getPieceColor(row, column + increment)) == Math.signum(color) && increment != -1;
    }

    private boolean checkMovementPrimaryDiagonal(int color, int row, int column) {
        int incrementRow = 1, incrementColumn = 1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == -Math.signum(color)) {
            incrementRow++;
            incrementColumn++;
        }
        if (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)
                && incrementRow != 1 && incrementColumn != 1) {
            return true;
        }

        incrementRow = -1;
        incrementColumn = -1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == -Math.signum(color)) {
            incrementRow--;
            incrementColumn--;
        }
        return checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)
                && incrementRow != -1 && incrementColumn != -1;
    }

    private boolean checkMovementSecondaryDiagonal(int color, int row, int column) {
        int incrementRow = 1, incrementColumn = -1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == -Math.signum(color)) {
            incrementRow++;
            incrementColumn--;
        }
        if (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)
                && incrementRow != 1 && incrementColumn != -1) {
            return true;
        }

        incrementRow = -1;
        incrementColumn = 1;
        while (checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == -Math.signum(color)) {
            incrementRow--;
            incrementColumn++;
        }
        return checkBounds(row, incrementRow) && checkBounds(column, incrementColumn) && Math.signum(board.getPieceColor(row + incrementRow, column + incrementColumn)) == Math.signum(color)
                && incrementRow != -1 && incrementColumn != 1;
    }
}
