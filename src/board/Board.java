package board;

public class Board {

    final private int SIZE = 8;
    private int[][] board;
    private int turn;
    private boolean updated;

    public Board() {
        this.board = new int[SIZE][SIZE];
        this.turn = -1;
        this.updated = false;
    }

    public Board(int size) {
        this.board = new int[size][size];
        this.turn = -1;
        this.updated = false;
    }

    public int getBoardSize() {
        return this.SIZE;
    }

    public int[][] getBoard() {
        return this.board;
    }

    public int getPieceColor(int row, int column) {
        return this.getBoard()[row][column];
    }

    public void setPieceColor(int row, int column, int value) {
        this.board[row][column] = value;
    }

    public int getTurn() {
        return this.turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void changeTurn() {
        this.turn *= -1;
    }

    public boolean wasUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public boolean isInBounds(int x, int y) {
        return ((x >= 0 && x < 8) && (y >= 0 && y < 8));
    }
}
