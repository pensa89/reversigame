package board;

import board.management.BoardManagement;

public class CheckVictory {

    public static int checkWin(Board stableBoard) {
        int sum = 0, boardSize = stableBoard.getBoardSize();
        BoardManagement boardManagement = new BoardManagement(stableBoard);

        if (!boardManagement.possibleMoves(stableBoard, 1).isEmpty() || !boardManagement.possibleMoves(stableBoard, -1).isEmpty()) {
            return Integer.MIN_VALUE;
        } else {
            for (int i = 0; i < boardSize; i++) {
                for (int j = 0; j < boardSize; j++) {
                    if (stableBoard.getPieceColor(i, j) != 0) {
                        sum += stableBoard.getPieceColor(i, j);
                    }
                }
            }
            if (sum == 0) {
                return sum;
            } else if (sum < 0) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
