package reversi.game;

import java.awt.Point;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import player.Computer;
import player.Player;

public class GameSpectate extends Game {

    public GameSpectate(Player player1, Player player2) {
        super(player1, player2);
    }

    @Override
    public void run() {
        startGame();
    }

    public void startGame() {
        while (gameOn) {
            Player p = players.get(controller.getBoard().getTurn());

            if (!player1.getCanMove() && !player2.getCanMove()) {
                gameOn = false;
                break;
            }

            Point nextAction = ((Computer) p).makeMoveComputer(controller.getBoard());

            if (nextAction != null) {
                p.setCanMove(true);
                p.makeMove(nextAction.x, nextAction.y);
            } else {
                JOptionPane.showMessageDialog(null, "Computer has no plays avaliable!", "", JOptionPane.INFORMATION_MESSAGE);
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(GameSpectate.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (!verifyGameOver()) {
                switch (gameOverResult) {
                    case -1:
                        JOptionPane.showMessageDialog(null, "Black won!", "Game over", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case 0:
                        JOptionPane.showMessageDialog(null, "Tie!", "Game over", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case +1:
                        JOptionPane.showMessageDialog(null, "White won!", "Game over", JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }
            controller.getBoardDrawing().repaint();
            controller.changeTurn();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(GameSpectate.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller.getWindow().dispatchEvent(new WindowEvent(controller.getWindow(), WindowEvent.WINDOW_CLOSING));
    }
}
