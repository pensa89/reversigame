package reversi.game;

import board.CheckVictory;
import board.management.PieceStability;
import controller.Controller;
import java.util.HashMap;
import java.util.Map;
import player.Player;

public abstract class Game implements Runnable {

    protected boolean gameOn;
    protected int gameOverResult;
    protected Player player1;
    protected Player player2;
    protected Map<Integer, Player> players = new HashMap();
    protected Controller controller;

    public Game(Player player1, Player player2) {
        this.gameOn = true;
        this.gameOverResult = Integer.MIN_VALUE;
        this.controller = Controller.getInstance();
        this.player1 = player1;
        this.player2 = player2;
        this.players.put(player1.getPlayerColor(), player1);
        this.players.put(player2.getPlayerColor(), player2);
    }

    public boolean verifyGameOver() {
        gameOverResult = CheckVictory.checkWin(PieceStability.stablePieces(Controller.getInstance().getBoard()));
        if (gameOverResult == -1 || gameOverResult == 0 || gameOverResult == 1) {
            gameOn = false;
        }
        return gameOn;
    }

    public abstract void startGame();

}
