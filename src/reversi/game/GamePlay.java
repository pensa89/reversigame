package reversi.game;

import java.awt.Point;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import player.Computer;
import player.Player;

public class GamePlay extends Game {

    public GamePlay(Player player1, Player player2) {
        super(player1, player2);
    }

    @Override
    public void run() {
        startGame();
    }

    @Override
    public void startGame() {
        controller.getBoardDrawing().setGameMode(true);
        while (gameOn) {
            Computer c = (Computer) player2;
            int currentTurn = controller.getBoard().getTurn();
            controller.getBoardDrawing().repaint();
            
            if (c.getPlayerColor() == currentTurn) {

                if (!player1.getCanMove() && !player2.getCanMove()) {
                    verifyGameOver();
                    switch (gameOverResult) {
                        case -1:
                            JOptionPane.showMessageDialog(controller.getWindow(), "Black won!");
                            break;
                        case 0:
                            JOptionPane.showMessageDialog(controller.getWindow(), "Tie!");
                            break;
                        case +1:
                            JOptionPane.showMessageDialog(controller.getWindow(), "White won!");
                            break;
                    }
                    break;
                }
                
                Point nextAction = c.makeMoveComputer(controller.getBoard());

                if (nextAction != null) {
                    c.setCanMove(true);
                    c.makeMove(nextAction.x, nextAction.y);
                }

                if (!verifyGameOver()) {
                    switch (gameOverResult) {
                        case -1:
                            JOptionPane.showMessageDialog(null, "Black won!", "Game over", JOptionPane.INFORMATION_MESSAGE);
                            break;
                        case 0:
                            JOptionPane.showMessageDialog(null, "Tie!", "Game over", JOptionPane.INFORMATION_MESSAGE);
                            break;
                        case +1:
                            JOptionPane.showMessageDialog(null, "White won!", "Game over", JOptionPane.INFORMATION_MESSAGE);
                            break;
                    }
                }
                controller.getBoardDrawing().repaint();
                controller.changeTurn();                
            }
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(GameSpectate.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller.getWindow().dispatchEvent(new WindowEvent(controller.getWindow(), WindowEvent.WINDOW_CLOSING));
    }
}
