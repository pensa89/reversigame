package reversi.game;

import controller.Controller;
import java.util.HashMap;
import java.util.Map;
import player.Computer;
import player.Human;
import player.Player;

public class GameManagement {

    private Player player1;
    private Player player2;
    private Map<Integer, Player> players = new HashMap();
    private Controller controller;
    private static GameManagement gameManagement = null;
    private final int EASY_DIFFICULTY = 4, MEDIUM_DIFFICULTY = 8;
    
    private GameManagement() {
        this.controller = Controller.getInstance();
        this.player1 = new Player(0);
        this.player2 = new Player(0);
    }
    
    public static GameManagement getInstance(){
        if(gameManagement == null){
            gameManagement = new GameManagement();
        }
        return gameManagement;
    }
    
    public Player getPlayer1()
    {
        return this.player1;
    }
    
    public Player getPlayer2()
    {
        return this.player2;
    }
    
    public void setComputerColor()
    {
        this.player1 = new Computer(1, MEDIUM_DIFFICULTY);
        this.player2 = new Computer(-1, EASY_DIFFICULTY);
        this.controller.getBoardDrawing().setGameMode(false);
        players.put(player1.getPlayerColor(), player1);
        players.put(player2.getPlayerColor(), player2);
    }
    
    public void setController(Controller c)
    {
        this.controller = c;
    }
    
    public void setHumanColor(int color)
    {        
        this.player1 = new Human(color);
        this.player2 = new Computer(color * -1, MEDIUM_DIFFICULTY);
        this.controller.getBoardDrawing().setGameMode(false);
        players.put(player1.getPlayerColor(), player1);
        players.put(player2.getPlayerColor(), player2);
    }   
}
