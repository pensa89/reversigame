package ai;

import board.Board;
import static board.CheckVictory.checkWin;
import board.management.BoardManagement;
import board.management.PieceManagement;
import board.management.PieceStability;
import controller.Controller;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Minimax {

    private Set<Board> exploredSet;

    public Minimax() {
        this.exploredSet = new HashSet();
    }

    public Point alfaBetaSearch(Board board, int depth) {

        Set<Point> actions = new BoardManagement(board).possibleMoves(board, board.getTurn());
        List<PairActionValue> pairsActionValue = new ArrayList();
        Iterator<Point> iterator = actions.iterator();
        exploredSet.add(board);

        //An action is a Point, which represents a position on the board.
        List<PairActionValue> maximumPairs = new ArrayList();
        PairActionValue bestActionValuePair = new PairActionValue(null, Integer.MIN_VALUE);
        maximumPairs.add(bestActionValuePair);
        for (int i = 0; i < actions.size(); i++) {
            Point currentAction = iterator.next();
            Board temp = Controller.getInstance().deepCopy(board);
            pairsActionValue.add(new PairActionValue(currentAction, minimumValue(makePlay(temp, currentAction), Integer.MIN_VALUE, Integer.MAX_VALUE, depth - 1)));
            
            //If a result of the minimumValue is better than the current, replace the current action.
            //If a result has the same value of the better action, add it to a list.
            if (pairsActionValue.get(i).getValue() > maximumPairs.get(0).getValue()) {
                maximumPairs.clear();
                maximumPairs.add(pairsActionValue.get(i));
            } else if (pairsActionValue.get(i).getValue() == maximumPairs.get(0).getValue()) {
                maximumPairs.add(pairsActionValue.get(i));
            }
        }
        exploredSet.clear();
        //Choose a random action.
        return maximumPairs.get(new Random().nextInt(maximumPairs.size())).getAction();
    }

    public int maximumValue(Board board, int alpha, int beta, int depth) {
        Board stableBoard = PieceStability.stablePieces(board);
        if (exploredSet.contains(board)) {
            return Integer.MIN_VALUE;
        }
        if (depth == 0 || checkWin(stableBoard) == board.getTurn()) {
            return utilityFunction(stableBoard);
        }
        exploredSet.add(board);
        stableBoard.changeTurn();
        board.changeTurn();
        int value = Integer.MIN_VALUE;
        Set<Point> actions = new BoardManagement(board).possibleMoves(board, board.getTurn());
        Iterator<Point> iterator = actions.iterator();

        for (int i = 0; i < actions.size(); i++) {
            Board temp = Controller.getInstance().deepCopy(board);
            value = Math.max(value, minimumValue(makePlay(temp, iterator.next()), alpha, beta, depth - 1));

            if (value >= beta) {
                return value;
            }
            alpha = Math.max(alpha, value);
        }
        return value;
    }

    public int minimumValue(Board board, int alpha, int beta, int depth) {
        Board stableBoard = PieceStability.stablePieces(board);
        if (exploredSet.contains(board)) {
            return Integer.MAX_VALUE;
        }
        if (depth == 0 || checkWin(stableBoard) == board.getTurn()) {
            return utilityFunction(stableBoard);
        }
        exploredSet.add(board);
        stableBoard.changeTurn();
        board.changeTurn();
        int value = Integer.MAX_VALUE;
        Set<Point> actions = new BoardManagement(board).possibleMoves(board, board.getTurn());
        Iterator<Point> iterator = actions.iterator();

        for (int i = 0; i < actions.size(); i++) {
            Board temp = Controller.getInstance().deepCopy(board);
            value = Math.min(value, maximumValue(makePlay(temp, iterator.next()), alpha, beta, depth - 1));

            if (value <= alpha) {
                return value;
            }
            beta = Math.min(beta, value);
        }
        return value;
    }

    public int utilityFunction(Board stableBoard) {
        int result = 0;
        int color = stableBoard.getTurn();
        int boardSize = stableBoard.getBoardSize();

        //Temporary values. Depends on Vinicius' algorithm.
        int stablePiece = 25 * color;
        int cornerWeight = 10, stablePieceWeight = 5, pieceWeight = 1;

        if (stableBoard.getPieceColor(0, 0) == color) {
            result += cornerWeight;
        }
        if (stableBoard.getPieceColor(0, boardSize - 1) == color) {
            result += cornerWeight;
        }
        if (stableBoard.getPieceColor(boardSize - 1, 0) == color) {
            result += cornerWeight;
        }
        if (stableBoard.getPieceColor(boardSize - 1, boardSize - 1) == color) {
            result += cornerWeight;
        }

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (stableBoard.getPieceColor(i, j) == color) {
                    result += pieceWeight;
                } else if (stableBoard.getPieceColor(i, j) == stablePiece) {
                    result += stablePieceWeight;
                }
            }
        }
        return result;
    }

    public Board makePlay(Board board, Point move) {
        Board newBoard = Controller.getInstance().deepCopy(board);

        PieceManagement pieceManagement = new PieceManagement();
        pieceManagement.flipPieces(newBoard, newBoard.getTurn(), (int) move.getX(), (int) move.getY());

        return newBoard;
    }
}
