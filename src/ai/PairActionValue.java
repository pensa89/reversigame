package ai;

import java.awt.Point;

public class PairActionValue {

    private Point action;
    private int value;

    public PairActionValue(Point action, int value) {
        this.value = value;
        this.action = action;
    }

    public int getValue() {
        return this.value;
    }

    public Point getAction() {
        return this.action;
    }
}
