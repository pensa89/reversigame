package player;

import ai.Minimax;
import board.Board;
import controller.Controller;
import java.awt.Point;

public class Computer extends Player {

    private Minimax minimax;
    private int difficulty;

    public Computer(int color, int dificulty) {
        super(color);
        this.minimax = new Minimax();
        this.difficulty = dificulty;
    }

    /**
     * Call to the minimax method.
     * @param board
     * @return Best action accordingly with the minimax algorithm.
     */
    public Point makeMoveComputer(Board board) {
        Board temp = Controller.getInstance().deepCopy(board);

        Point nextAction = minimax.alfaBetaSearch(temp, difficulty);

        return nextAction;
    }

    public int getDifficulty() {
        return this.difficulty;
    }
}
