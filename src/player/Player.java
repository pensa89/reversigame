package player;

import board.management.PieceManagement;
import controller.Controller;

public class Player {

    private int color;
    private boolean canMove;
    private PieceManagement pieceManagement;

    public Player(int color) {
        this.color = color;
        this.canMove = true;
        this.pieceManagement = new PieceManagement();
    }

    public int getPlayerColor() {
        return this.color;
    }

    public boolean getCanMove() {
        return this.canMove;
    }

    public void setCanMove(boolean value) {
        this.canMove = value;
    }

    public boolean makeMove(int row, int column) {
        pieceManagement.flipPieces(Controller.getInstance().getBoard(), this.getPlayerColor(), row, column);
        return true;
    }
}
