package player;

import controller.Controller;
import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

public class Human extends Player {

    public Human(int color) {
        super(color);
    }

    public Set<Point> makeMoveHuman() {
        Set<Point> possibleMoves = new HashSet();
        possibleMoves = (Controller.getInstance().getBoardManagement()).possibleMoves(Controller.getInstance().getBoard(), this.getPlayerColor());

        return possibleMoves;
    }
}
