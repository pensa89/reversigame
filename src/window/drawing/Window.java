package window.drawing;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import reversi.game.Game;
import reversi.game.GameManagement;
import reversi.game.GamePlay;
import reversi.game.GameSpectate;

public class Window extends JFrame implements ActionListener {

    private Controller controller;
    private GameManagement gameManagement;
    private JButton blackButton, whiteButton, startGameButton, spectateButton;

    public Window() throws IOException {
        this.gameManagement = GameManagement.getInstance();
        this.controller = Controller.getInstance();
        this.gameManagement.setController(controller);
        this.controller.setGameManagement(gameManagement);
        this.controller.setWindow(this);
        initialize();
    }

    private void initialize() {
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

        add(createColorButtons(), BorderLayout.SOUTH);
        add(createGameButtons(), BorderLayout.NORTH);
        add(new BoardDrawing(), BorderLayout.CENTER);
        pack();
        setVisible(true);
    }        

    private JPanel createColorButtons() {
        JPanel panel = new JPanel();
        this.blackButton = new JButton("Black");
        this.blackButton.addActionListener(this);
        this.whiteButton = new JButton("White");
        this.whiteButton.addActionListener(this);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(blackButton);
        panel.add(whiteButton);
        return panel;
    }

    private JPanel createGameButtons() {
        JPanel panel = new JPanel();
        this.startGameButton = new JButton("Start game");
        this.startGameButton.addActionListener(this);
        this.spectateButton = new JButton("Spectate a game");
        this.spectateButton.addActionListener(this);
        ButtonGroup gameButtonsGroup = new ButtonGroup();
        gameButtonsGroup.add(startGameButton);
        gameButtonsGroup.add(spectateButton);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(startGameButton);
        panel.add(spectateButton);
        return panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Start game":
                if (gameManagement.getPlayer1().getPlayerColor() != 0) {
                    this.spectateButton.setEnabled(false);
                    this.startGameButton.setEnabled(false);
                    this.blackButton.setEnabled(false);
                    this.whiteButton.setEnabled(false);
                    Game g = new GamePlay(gameManagement.getPlayer1(), gameManagement.getPlayer2());
                    Thread t1 = new Thread(g);
                    t1.start();
                }
                break;
            case "Spectate a game":
                this.spectateButton.setEnabled(false);
                this.startGameButton.setEnabled(false);
                this.blackButton.setEnabled(false);
                this.whiteButton.setEnabled(false);
                this.gameManagement.setComputerColor();
                Game g = new GameSpectate(gameManagement.getPlayer1(), gameManagement.getPlayer2());
                Thread t1 = new Thread(g);
                t1.start();
                break;
            case "Black":
                gameManagement.setHumanColor(-1);
                break;
            case "White":
                gameManagement.setHumanColor(1);
                break;
        }
    }
}
