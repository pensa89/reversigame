package window.drawing;

import controller.Controller;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import player.Human;

public class BoardDrawing extends JPanel {

    private boolean gameMode = false;
    private Controller controller;
    private BufferedImage reversiBoard;
    private BufferedImage whitePiece, blackPiece;
    private int[][] board;

    public BoardDrawing() {
        super();
        try {
            this.reversiBoard = ImageIO.read(getClass().getResource("/images/reversi.png"));            
            this.whitePiece = ImageIO.read(getClass().getResource("/images/whitePiece.png"));   
            this.blackPiece = ImageIO.read(getClass().getResource("/images/blackPiece.png"));   
        } catch (IOException ex) {
            Logger.getLogger(BoardDrawing.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller = Controller.getInstance();
        controller.setBoardDrawing(this);
        gameMode = false;
        createListener();
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 300);
    }

    public void setGameMode(boolean b) {
        this.gameMode = b;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.board = Controller.getInstance().getBoard().getBoard();
        g.drawImage(reversiBoard, 0, 0, null);
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (Math.signum(board[i][j]) == 1) {
                    g.drawImage(whitePiece, i * 36 + 9, j * 36 + 9, whitePiece.getWidth(), whitePiece.getHeight(), null);
                } else if (Math.signum(board[i][j]) == -1) {
                    g.drawImage(blackPiece, i * 36 + 9, j * 36 + 9, blackPiece.getWidth(), blackPiece.getHeight(), null);
                }
            }
        }
    }

    private void createListener() {

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int currentTurn = controller.getBoard().getTurn();
                if (gameMode && currentTurn == controller.getGameManagement().getPlayer1().getPlayerColor()) {
                    int row = (e.getX() - 9) / 36;
                    int column = (e.getY() - 9) / 36;
                    Point nextAction = new Point(row, column);

                    Human player = (Human) controller.getGameManagement().getPlayer1();
                    Set<Point> possibleMoves = player.makeMoveHuman();

                    if (!possibleMoves.isEmpty() && possibleMoves.contains(nextAction)) {  
                        controller.getGameManagement().getPlayer1().makeMove(row, column);
                        controller.getBoardDrawing().repaint();
                        controller.changeTurn();
                    } else if(possibleMoves.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "You have no plays avaliable!", "", JOptionPane.INFORMATION_MESSAGE);
                        controller.changeTurn();
                    }
                }
            }
        });
    }
}
