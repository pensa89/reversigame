package window.drawing;

import java.io.IOException;

public class Main {

    //Throughout out program, a positive number represents a white piece, a negative number represents a black piece
    // and the zero represents a empty position.
    public static void main(String[] args) throws IOException {
        new Window();
    }
}
