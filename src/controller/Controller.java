package controller;

import board.Board;
import board.management.BoardManagement;
import java.awt.Window;
import reversi.game.GameManagement;
import window.drawing.BoardDrawing;

public class Controller {

    private Board board;
    private static Controller controller;
    private BoardManagement boardManagement;
    private GameManagement gameManagement;
    private Window window;
    private BoardDrawing boardDrawing;

    private Controller() {
        this.board = new Board();
        this.initializeBoard();
        boardManagement = new BoardManagement(this.board);
    }

    public static Controller getInstance() {
        if (controller == null) {
            controller = new Controller();
        }
        return controller;
    }

    public Board getBoard() {
        return this.board;
    }

    public BoardManagement getBoardManagement() {
        return this.boardManagement;
    }

    public void setBoard(Board newBoard) {
        this.board = newBoard;
    }

    public Window getWindow() {
        return this.window;
    }

    public BoardDrawing getBoardDrawing() {
        return this.boardDrawing;
    }

    public GameManagement getGameManagement() {
        return this.gameManagement;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public void setBoardDrawing(BoardDrawing boardDrawing) {
        this.boardDrawing = boardDrawing;
    }

    public void setGameManagement(GameManagement gm) {
        this.gameManagement = gm;
    }
    
    public void changeTurn() {
        board.setTurn(board.getTurn() * (-1));
    }

    public Board deepCopy(Board b) {
        Board newBoard = new Board();
        newBoard.setTurn(b.getTurn());
        for (int i = 0; i < b.getBoardSize(); i++) {
            for (int j = 0; j < b.getBoardSize(); j++) {
                newBoard.setPieceColor(i, j, b.getPieceColor(i, j));
            }
        }
        return newBoard;
    }

    public void initializeBoard() {
        this.board.setPieceColor(3, 3, 1);
        this.board.setPieceColor(4, 4, 1);
        this.board.setPieceColor(3, 4, -1);
        this.board.setPieceColor(4, 3, -1);
    }
}
